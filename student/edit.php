
<?php

//db connection

$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

$stmt = $db->query($query);
$student = $stmt->fetch(PDO::FETCH_ASSOC);

$query="SELECT id, title FROM `courses` ORDER BY title ASC ";
$stmt=$db->query($query);
$courses=$stmt->fetchAll(PDO::FETCH_ASSOC);

$query = "SELECT course_id FROM `map_courses_students` WHERE student_id =".$student['id'];
$stmt = $db->query($query);
$arr_courses = $stmt->fetchAll(PDO::FETCH_ASSOC);

$chosen_courses = "";
foreach ($arr_courses as $chosen_course) {

    $cquery = "SELECT * FROM `courses` WHERE id =".$chosen_course['course_id'];
    $stmt = $db->query($cquery);
    $chosen_courses[] = $stmt->fetch(PDO::FETCH_ASSOC)['id'];
}

//print_r($chosen_courses);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit</title>

    <!-- Bootstrap -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">

    <div class="container-fluid well">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
               <!-- <form action="update.php?id=<?=$student['id']?>" method="post"> -->
                <form action="update.php" method="post">
                    <input value="<?=$student['id'];?>" id="id" type="hidden" name="id" class="form-control" placeholder="Enter Your First Name">


                    <div class="form-group">
                        <label for="first_name" >First Name :</label>
                        <input value="<?=$student['first_name'];?>" id="first_name"type="text" name="first_name" class="form-control" placeholder="Enter Your First Name">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name :</label>
                        <input value="<?=$student['last_name'];?>" type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                    </div>

                    <div class="form-group">
                        <label for="seip">SEIP ID</label>
                        <input value="<?=$student['seip'];?>" type="text"  id="seip" name="seip" class="form-control" placeholder="Enter Your SEIP ID">
                    </div>

                    <div class="form-group">
                        <label for="courses">Courses</label>
                        <select name="courses[]" multiple="multiple" size="10">
                            <option>Choose Courses</option>
                            <?php
                            $selected = '';
                            foreach($courses as $course):
                                if(in_array($course['id'],$chosen_courses )){
                                    $selected = 'selected="selected"';
                                }else{
                                    $selected = '';
                                }
                                ?>
                                <option value="<?=$course['id']?>" <?=$selected;?>><?=$course['title'];?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>
                </form>

            </div>
        </div>
    </div>






    <script src="../asset/jquery/jquery-3.2.0.min.js"></script>

    <script src="../asset/js/bootstrap.min.js"></script>
</body>
</html>