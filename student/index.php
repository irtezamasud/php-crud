<?php

//db connection

$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query = "SELECT * FROM `students` ORDER BY id DESC";

$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Show student index</title>

    <!-- Bootstrap -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">

<div class="container-fluid well">
    <div class="row">

        <nav>
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Add new<span class="caret"></span></button>
                <ul class="dropdown-menu">
            <li><a href="create.php">New Student</a></li>
            <li><a href="assign.php">New Course</a></li>
                </ul>
            </div>
        </nav>

        <div class="col-md-offset-3 col-md-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>SEIP</th>
                        <th>Courses</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php

                foreach($students as $student):
                    $query = "SELECT course_id FROM `map_courses_students` WHERE student_id =".$student['id'];
                    $stmt = $db->query($query);
                    $arr_courses = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    $courses = "";
                    foreach ($arr_courses as $course) {

                        $cquery = "SELECT title FROM `courses` WHERE id =".$course['course_id'];
                        $stmt = $db->query($cquery);
                        $courses[] = $stmt->fetch(PDO::FETCH_ASSOC)['title'];

                    }
                ?>

                    <tr>
                        <td><?= $student['id'] ?></td>
                        <td><?php echo $student['first_name'] ?></td>
                        <td><?= $student['last_name'] ?></td>
                        <td><?= $student['seip'] ?></td>
                        <td><?= is_array($courses)?implode(",",$courses):"No Course is assigned."; ?></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Modify<span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="show.php?id=<?=$student['id'];?>">Show</a></li>
                                    <li><a href="edit.php?id=<?=$student['id'];?>">Edit</a></li>
                                    <li><a href="delete.php?id=<?=$student['id'];?>">Delete</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php
                endforeach;
                ?>

                </tbody>
            </table>

        </div>
    </div>
</div>






<script src="../asset/jquery/jquery-3.2.0.min.js"></script>

<script src="../asset/js/bootstrap.min.js"></script>

</body>
</html>