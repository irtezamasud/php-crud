<?php
//DB Connection
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query="SELECT id, title FROM `courses` ORDER BY title ASC ";

$stmt=$db->query($query);
$courses=$stmt->fetchAll(PDO::FETCH_ASSOC);
//print_r($courses);

$query="SELECT id, first_name, last_name 
FROM `students` ORDER BY first_name ASC ";

$stmt=$db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
//print_r($students);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Assign courses</title>

    <!-- Bootstrap -->

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">
<div class="container-fluid well">

    <div class="row">

        <div class="col-md-offset-3 col-md-6">
            <nav>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Add<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="../course/create.php">New Courses</a> </li>
                    </ul>
                </div>
                <br>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">View<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="../course/index.php">View all Courses</a> </li>
                        <li><a href="index.php">View all students</a> </li>
                    </ul>
                </div>

            </nav>

            <form action="assign_students_courses.php" method="post">
                <select name="students">
                    <option>Choose a Student</option>
                    <?php
                    foreach($students as $student):
                    ?>
                    <option value="<?=$student['id']?>"><?=$student['first_name']." ".$student['last_name']?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
                <select name="courses[]" multiple="multiple" size="10">
                    <option>Choose Courses</option>
                    <?php
                    foreach($courses as $course):
                        ?>
                        <option value="<?=$course['id']?>"><?=$course['title'];?></option>
                        <?php
                    endforeach;
                    ?>
                </select>
                <button type="submit">Save</button>
            </form>

        </div>

    </div>

</div>

<script src="../asset/jquery/jquery-3.2.0.min.js"></script>

<script src="../asset/js/bootstrap.min.js"></script>


</body>
</html>
