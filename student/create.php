<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student details</title>

    <!-- Bootstrap -->
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">

    <div class="container-fluid well">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <form action="store.php" method="post">

                    <div class="form-group">
                        <label for="first_name" >First Name :</label>
                        <input id="first_name"type="text" name="first_name" class="form-control" placeholder="Enter Your First Name">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name :</label>
                        <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                    </div>

                    <div class="form-group">
                        <label for="seip">SEIP ID</label>
                        <input type="text"  id="seip" name="seip" class="form-control" placeholder="Enter Your SEIP ID">
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>

                </form>

            </div>
        </div>
    </div>






    <script src="../asset/jquery/jquery-3.2.0.min.js"></script>

    <script src="../asset/js/bootstrap.min.js"></script>
</body>
</html>