<?php
//DB Connection
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query="SELECT * FROM `courses` ORDER BY id DESC ";

$stmt=$db->query($query);
$courses=$stmt->fetchAll(PDO::FETCH_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Show courses</title>

    <!-- Bootstrap -->

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">
<div class="container-fluid well">

    <div class="row">



            <nav>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Add new<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="create.php">New Courses</a> </li>
                        <li><a href="../student/index.php">All Students</a> </li>
                    </ul>
                </div>
            </nav>
            <div class="col-md-offset-3 col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th> id         </th>
                    <th> Course Code </th>
                    <th> Course Title  </th>
                    <th> Actions      </th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($courses as $course) {


                    ?>

                    <tr>
                        <td><?= $course['id'] ?></td>
                        <td><?php echo $course['code']?></td>
                        <td><?= $course['title'] ?></td>


                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Modify<span class="caret"></span></button>
                                <ul class="dropdown-menu">

                                <li><a href="show.php?id=<?= $course['id'] ?>">Show</a></li>
                                <li><a href="edit.php?id=<?= $course['id'] ?>">Edit</a> </li>
                                <li><a href="delete.php?id=<?= $course['id'] ?>">Delete</a></li>
                                </ul>
                        </td>
                    </tr>

                    <?php
                };
                ?>
                </tbody>
            </table>

        </div>

    </div>

</div>

<script src="../asset/jquery/jquery-3.2.0.min.js"></script>

<script src="../asset/js/bootstrap.min.js"></script>

</body>
</html>
