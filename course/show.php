<?php
//DB Connection
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query="SELECT * FROM `courses` WHERE id =".$_GET['id'];

$stmt= $db->query($query);
$course = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Course</title>

    <!-- Bootstrap -->

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">
<div class="container-fluid well">

    <div class="row">

        <nav>
            <ul>
            <li><a href="index.php"> All courses</a></li>
            </ul>
        </nav>

        <div class="col-md-offset-3 col-md-6">

            <dl>
                <dt>  ID  </dt>
                <dd><?=$course['id'];?> </dd>


                <dt>  Subject Code  </dt>
                <dd> <?=$course['code'];?> </dd>

                <dt>  Subject Title  </dt>
                <dd> <?=$course['title'];?>  </dd>
            </dl>

        </div>

    </div>

</div>

<script src="../asset/jquery/jquery-3.2.0.min.js"></script>

<script src="../asset/js/bootstrap.min.js"></script>

</body>
</html>
