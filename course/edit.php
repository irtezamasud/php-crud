<?php
//DB Connection
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');

$query="SELECT * FROM `courses` WHERE id =".$_GET['id'];

$stmt=$db->query($query);
$course =$stmt->fetch(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>edit</title>

    <!-- Bootstrap -->

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">


</head>
<body style="background-image: url(../images/css.jpg)">
<div class="container-fluid well">

    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <form action="update.php?id=<?= $course['id'] ?>" method="post">
                <fieldset>
                    <legend>Edit Course Information</legend>
                    <div class="form-group">
                        <label for="Subject_Code">Course Code</label>
                        <input value="<?=$course['code']?>" type="number" class="form-control" id="code" name="code" placeholder="Course Code">
                    </div>
                    <div class="form-group">
                        <label for="Subject_Title">Title</label>
                        <input value="<?=$course['title']?>" type="text" class="form-control" id="title" name="title" placeholder="Course Title">
                    </div>


                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                </fieldset>
            </form>

        </div>

    </div>

</div>
<script src="../asset/jquery/jquery-3.2.0.min.js"></script>

<script src="../asset/js/bootstrap.min.js"></script>
</body>
</html>
