
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Homepage</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">

</head>
<body style="background-image: url(images/css.jpg)">

<div class="jumbotron text-center">
    <h1>Student course management</h1>
    <p>CRUD</p>
</div>
<div class="container">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">ADD
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="student/create.php">Add Student</a></li>
            <li><a href="course/create.php">Add Course</a></li>
        </ul>
    </div>
    <br>
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">View
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="student/index.php">All students</a></li>
            <li><a href="course/index.php">All Courses</a></li>
        </ul>
    </div>
</div>


<script src="asset/jquery/jquery-3.2.0.min.js"></script>

<script src="asset/js/bootstrap.min.js"></script>
</body>
</html>
